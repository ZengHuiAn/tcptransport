package main

import "sync"

func main() {
	//
	group := sync.WaitGroup
	go func() {
		group.Done()
	}()

	group.wait()
}
