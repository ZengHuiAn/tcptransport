package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"github.com/AnZengHui/tcptransport/src/framework/Loger"
	sig "github.com/AnZengHui/tcptransport/src/framework/signal"
	"github.com/AnZengHui/tcptransport/src/proxyServer/network"
	"net"
	"os"
)

func main() {
	raddr := net.TCPAddr{Port: 10000}
	Loger.SetFilePath("log/client")
	conn, err := net.DialTCP("tcp", nil, &raddr)
	var buffer bytes.Buffer
	header := network.NewClientHeader(101, 2)
	err = binary.Write(&buffer, binary.BigEndian, header)

	fmt.Println("send", buffer.Bytes())
	if err != nil {
		panic(fmt.Sprintf("connect error :%v", err))
	}
	client := network.NewConnectClient(conn)
	go client.StartUp()
	go client.HandleConnect()
	go func() {
		header := network.NewClientHeader(101, 2)
		var buffer bytes.Buffer
		err = binary.Write(&buffer, binary.BigEndian, header)

		fmt.Println("send", buffer.Bytes())
		client.SendData(buffer.Bytes())
	}()
	sig.Wait(os.Interrupt, os.Kill)
	client.Ptr.Close()
	//conn()

}
