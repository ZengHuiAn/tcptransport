package main

import (
	"bytes"
	"fmt"
	"github.com/AnZengHui/tcptransport/src/framework/decode"
)

func main() {
	var buff bytes.Buffer
	arr := make([]interface{}, 1)
	arr[0] = 100000.000111111

	fmt.Println(arr[0])
	_, _ = decode.Encode(&buff, arr)
	fmt.Println(buff.Bytes())
	values, _ := decode.Decode(&buff)
	fmt.Println(values.([]interface{}))
}
