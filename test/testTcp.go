package main

import (
	"fmt"
	"net"
	"os"
	"time"
)

const (
	MaxConnNum = 100000
)

func EchoFunc(conn net.Conn) {
	defer conn.Close()
	buf := make([]byte, 10)
	for {
		_, err := conn.Read(buf)
		if err != nil {
			//println("Error reading:", err.Error())
			return
		}
		//fmt.Printf("server %s\n", string(buf))
		//send reply
		_, err = conn.Write(buf)
		if err != nil {
			//println("Error send reply:", err.Error())
			return
		}
	}
}

func main() {
	listener, err := net.Listen("tcp", "127.0.0.1:8088")
	if err != nil {
		fmt.Println("error listening:", err.Error())
		os.Exit(1)
	}
	defer listener.Close()

	fmt.Printf("running ...\n")

	var curConnNum int = 0
	conn_chan := make(chan net.Conn)
	chConnChange := make(chan int)

	go func() {
		for conn_change := range chConnChange {
			curConnNum += conn_change
			fmt.Println(curConnNum)
		}
	}()

	go func() {
		for _ = range time.Tick(5e9) {
			fmt.Printf("cur conn num: %d\n", curConnNum)
		}
	}()

	for i := 0; i < MaxConnNum; i++ {
		go func() {
			for conn := range conn_chan {
				chConnChange <- 1
				EchoFunc(conn)
				chConnChange <- -1
			}
		}()
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			println("Error accept:", err.Error())
			return
		}
		conn_chan <- conn
	}
}
