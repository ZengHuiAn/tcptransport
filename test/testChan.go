package main

import (
	"fmt"
	"github.com/AnZengHui/tcptransport/src/framework/syncGoroutine"
	"net"
	"runtime"
	"time"
)

func main() {
	listen, _ := net.ListenTCP("tcp", &net.TCPAddr{net.ParseIP("127.0.0.1"), 6666, ""})

	println(runtime.NumGoroutine())

	for {
		conn, _ := listen.AcceptTCP()
		go test(conn)
	}

	fmt.Println(runtime.NumGoroutine())
}

func test(conn *net.TCPConn) {

	pool := syncGoroutine.New(1)
	for i := 0; i < 1000; i++ {
		pool.Add(1)
		go func() {
			time.Sleep(time.Second)
			println(runtime.NumGoroutine())
			pool.Done()
		}()
	}
	pool.Wait()
}
