package main

import (
	"flag"
	"fmt"

	"github.com/logrusorgru/aurora"
)

// colorizer
var au aurora.Aurora

var colors = flag.Bool("colors", false, "enable or disable colors")

func init() {
	flag.Parse()
	au = aurora.NewAurora(*colors)
}

func main() {
	// use colorizer
	fmt.Println()
}
