package main

import (
	"fmt"
	"github.com/AnZengHui/tcptransport/src/framework/Subscribe"
)

func main() {
	dele := Subscribe.NewDelegate()
	dele.AddListener(func(params ...interface{}) {
		fmt.Println("调用函数")
		dele.RemoveListener(params[0].(*Subscribe.Event))
	})
	dele.Invoke("123")
	dele.Invoke("123")
}
