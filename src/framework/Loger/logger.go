package Loger

import (
	"fmt"
	"io"
	lg "log"
	"os"
	"time"
)

const (
	PRINT_LEVEL_MIN = iota
	PRINT_LEVEL_ERROR
	PRINT_LEVEL_WARN
	PRINT_LEVEL_DEBUG
	PRINT_LEVEL_INFO
	PRINT_LEVEL_MAX
)

var PrintLevel int = PRINT_LEVEL_MAX
var OutputFilePrefix string = "log"
var OutputFileCreateTime = time.Date(1987, 1, 1, 0, 0, 0, 0, time.Now().Location())

var filePath = ""

func init() {
	lg.SetFlags(lg.LstdFlags)
}
func SetFilePath(_filepath string) {
	filePath = _filepath
	_, err := os.Stat(filePath)
	if os.IsNotExist(err) {
		err = os.MkdirAll(filePath, os.ModePerm)

		if err != nil {
			fmt.Println("创建文件夹失败：", err)
		}
	}
}

func SetOutput(w io.Writer) {
	lg.SetOutput(w)
	OutputFilePrefix = ""
	OutputFileCreateTime = time.Date(1987, 1, 1, 0, 0, 0, 0, time.Now().Location())
}
func SetOutputFile(prefix string) {
	// prepare & check
	now := time.Now()
	if now.Year() == OutputFileCreateTime.Year() && now.Month() == OutputFileCreateTime.Month() && now.Day() == OutputFileCreateTime.Day() {
		return
	}
	path := fmt.Sprintf("%s/%04d%02d%02d.log", filePath, now.Year(), now.Month(), now.Day())

	// open & set
	if file, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666); err != nil {
		lg.Printf("Fail to redirect to %s\n", path)
	} else {
		//lg.Printf("Success to redirect to %s\r\n", path)
		lg.SetOutput(file)
		OutputFilePrefix = prefix
		OutputFileCreateTime = now
	}
}
func TryRedirectOutputFile() {
	if len(OutputFilePrefix) != 0 {
		SetOutputFile(OutputFilePrefix)
	}
}
func SetPrintLevel(lv int) {
	if lv <= PRINT_LEVEL_MIN {
		PrintLevel = PRINT_LEVEL_MIN
	} else if lv >= PRINT_LEVEL_MAX {
		PrintLevel = PRINT_LEVEL_MAX
	} else {
		PrintLevel = lv
	}
}
func Error(format string, v ...interface{}) {
	if PrintLevel >= PRINT_LEVEL_ERROR {
		TryRedirectOutputFile()
		var strFmt = "[ ERROR ]" + format + "\n"
		lg.Printf(strFmt, v...)
		if Mode {
			fmt.Print(getPath(), Red(fmt.Sprintf(strFmt, v...)))
		}
	}
}

func Warn(format string, v ...interface{}) {
	if PrintLevel >= PRINT_LEVEL_WARN {
		TryRedirectOutputFile()
		var strFmt = "[ WARN ]" + format + "\n"
		lg.Printf(strFmt, v...)
		if Mode {
			fmt.Print(getPath(), Yellow(fmt.Sprintf(strFmt, v...)))
		}
	}
}

func Debug(format string, v ...interface{}) {
	if PrintLevel >= PRINT_LEVEL_DEBUG {
		TryRedirectOutputFile()
		var strFmt = "[ DEBUG ]" + format + "\n"
		lg.Printf(strFmt, v...)
		if Mode {
			fmt.Print(getPath(), Blue(fmt.Sprintf(strFmt, v...)))
		}
	}

}

func Info(format string, v ...interface{}) {
	if PrintLevel >= PRINT_LEVEL_INFO {
		TryRedirectOutputFile()
		var strFmt = "[ INFO ]" + format + "\n"
		lg.Printf(strFmt, v...)
		if Mode {
			fmt.Print(getPath(), Cyan(fmt.Sprintf(strFmt, v...)))
		}
	}

}
func Printf(format string, v ...interface{}) {
	TryRedirectOutputFile()
	var strFmt = format + "\n"
	lg.Printf(strFmt, v...)
	if Mode {
		fmt.Print(getPath(), Green(fmt.Sprintf(strFmt, v...)))
	}

}

func getPath() string {
	now := time.Now()
	path := fmt.Sprintf("%04d/%02d/%02d %02d:%02d:%2d\t", now.Year(), now.Month(), now.Day(), now.Hour(), now.Minute(), now.Second())
	return path
}

func Println(v ...interface{}) {
	TryRedirectOutputFile()
	lg.Println(v...)
	if Mode {
		fmt.Println(getPath(), Black(fmt.Sprintln(v...)))
	}
}

var Mode = true

func ChangeMode(stats bool) {
	Mode = stats
}
