package Loger

import (
	"github.com/logrusorgru/aurora"
)

func Black(msg string) string {
	return aurora.Sprintf(aurora.Black(msg))
}

func Red(msg string) string {
	return aurora.Sprintf(aurora.Red(msg))
}

func Green(msg string) string {
	return aurora.Sprintf(aurora.Green(msg))
}

func Yellow(msg string) string {
	return aurora.Sprintf(aurora.White(msg))
}

func Blue(msg string) string {
	return aurora.Sprintf(aurora.Blue(msg))
}

func Magenta(msg string) string {
	return aurora.Sprintf(aurora.Magenta(msg))
}

func Cyan(msg string) string {
	return aurora.Sprintf(aurora.Cyan(msg))
}

func White(msg string) string {
	return aurora.Sprintf(aurora.White(msg))
}

func WhiteFormat(format string, v ...interface{}) string {
	return aurora.Sprintf(aurora.White(format), aurora.Red(v))
}
