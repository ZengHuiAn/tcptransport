package Subscribe

import (
	"container/list"
	"sync"
)

type Message struct {
	MessageID   int32 //消息ID
	MessageBody []byte
}

type Event struct {
	action func(params ...interface{})
}

type IDispatcher interface {
	Invoke(params ...interface{})
	AddListener(event Event)
}

type Delegate struct {
	eventList *list.List
	sync.Mutex
	IDispatcher
	ch chan []interface{}
}

func NewDelegate() *Delegate {

	deleteItem := &Delegate{}
	deleteItem.eventList = list.New()
	deleteItem.eventList.Init()
	deleteItem.ch = make(chan []interface{})
	return deleteItem
}

func (self Delegate) Run() {
	for {
		select {
		case <-self.ch:
		}
	}
}

func (self Delegate) Invoke(params ...interface{}) {
	self.Lock()
	defer self.Unlock()

	if self.eventList.Len() == 0 {
		return
	}

	for e := self.eventList.Front(); e != nil; e = e.Next() {
		event := e.Value.(*Event)
		news := []interface{}{event}
		args := append(news, params...)
		event.action(args...)
	}

}

func (self Delegate) AddListener(action func(params ...interface{})) {
	self.Lock()
	defer self.Unlock()
	event := &Event{action: action}
	self.eventList.PushFront(event)
}

func (self Delegate) RemoveListener(event *Event) {
	for e := self.eventList.Front(); e != nil; e = e.Next() {
		if e.Value == event {
			self.eventList.Remove(e)
		}
	}
}
