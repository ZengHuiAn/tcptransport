package Progress

import (
	"context"
	"fmt"
	proto "github.com/AnZengHui/tcptransport/src/framework/serverProto"
	"io"
	"log"
	"time"
)

type TransmissionClientService struct {
	client             proto.MessageServiceClient
	status             bool
	TransmissionStream proto.MessageService_TransmissionClient
}

func (service *TransmissionClientService) OnTransmission(client proto.MessageServiceClient) error {
	stream, err := service.client.Transmission(context.Background())
	if err != nil {
		fmt.Println("error Transmission ", err)
		return err
	}
	service.TransmissionStream = stream
	service.status = true
	defer func() {
		service.status = false
		service.TransmissionStream = nil
	}()

	for {
		resp, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}

		log.Printf("resp: value:  %v", resp)
		time.Sleep(time.Duration(1) * time.Second)
	}
	return nil
}

func (service *TransmissionClientService) SendMessage(msg *proto.MessageResponse) {
	err := service.TransmissionStream.Send(msg)
	if err != nil {
		log.Println("SendMessage:", err)
	}

}
