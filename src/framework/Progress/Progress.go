package Progress

import (
	"fmt"
	proto "github.com/AnZengHui/tcptransport/src/framework/serverProto"
	"io"
	"log"
)

type Streamer struct {
	transmissionServer proto.MessageService_TransmissionServer
	interactionServer  proto.MessageService_InteractionServer
}

func (s *Streamer) Transmission(stream proto.MessageService_TransmissionServer) error {
	s.transmissionServer = stream
	log.Println(" transmissionServer 启动了")
	for {
		//
		request, err := s.transmissionServer.Recv()
		fmt.Println(request)
		if err == io.EOF {
			return nil
		}
		go func() {
			_ = s.transmissionServer.Send(&proto.MessageRequest{Sn: request.Sn + 1})
		}()
		if err != nil {
			return err
		}
		log.Printf("TransmissionStream.Recv value: %v", request)
	}

	return nil
}
func (s *Streamer) Interaction(stream proto.MessageService_InteractionServer) error {
	s.interactionServer = stream
	log.Println(" interactionServer 启动了")
	return nil
}

func NewServer() *Streamer {
	return &Streamer{}
}
