package main

import (
	"context"
	"fmt"
	proto "github.com/AnZengHui/tcptransport/src/framework/serverProto"
	"google.golang.org/grpc"
	"io"
	"log"
	"time"
)

func main() {
	Port := "11000"
	conn, err := grpc.Dial("127.0.0.1:"+Port, grpc.WithInsecure())

	if err != nil {
		log.Fatalf("error Dial %v", err)
	}

	defer conn.Close()
	client := proto.NewMessageServiceClient(conn)
	err = OnTransmission(client)

}

func OnTransmission(client proto.MessageServiceClient) error {
	stream, err := client.Transmission(context.Background())
	if err != nil {
		fmt.Println("error Transmission ", err)
		return err
	}
	r := &proto.MessageResponse{}
	for {
		go func() {
			_ = stream.Send(r)
		}()
		resp, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}

		log.Printf("resp: value:  %v", resp)
		time.Sleep(time.Duration(1) * time.Second)
	}
	return nil
}
