package main

import (
	"fmt"
	"github.com/AnZengHui/tcptransport/src/framework/Progress"
	proto "github.com/AnZengHui/tcptransport/src/framework/serverProto"
	"google.golang.org/grpc"
	"log"
	"net"
)

var Port = "11000"

func main() {
	gServer := grpc.NewServer()

	proto.RegisterMessageServiceServer(gServer, Progress.NewServer())

	list, err := net.Listen("tcp", ":"+Port)
	if err != nil {
		log.Fatalf("error listen %v", err)
	}
	fmt.Println("grpc start")
	err = gServer.Serve(list)
	if err != nil {
		log.Fatalf("error bind %v", err)
	}
}
