package main

import (
	"context"
	"github.com/coreos/etcd/clientv3"
	"log"
	"time"
)

var v3Config = clientv3.Config{
	Endpoints:   []string{"localhost:2378"},
	DialTimeout: 5 * time.Second,
}

func main() {
	// 连接 etcd 一旦client创建成功，我们就不用再关心后续底层连接的状态了，client内部会重连。
	cli, err := clientv3.New(v3Config)

	if err != nil {
		log.Fatal("connect etcd error", err)
	}
	//创建一个key - value 对象
	kv := clientv3.NewKV(cli)

	putResp, err := kv.Put(context.TODO(), "/test/a", "something")
	println("putResp:", putResp)
	etResp, err := kv.Get(context.TODO(), "/test/a")
	println("etResp:", etResp)
}
