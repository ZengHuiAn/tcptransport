package main

import (
	"fmt"
	"github.com/AnZengHui/tcptransport/src/framework/Loger"
	sig "github.com/AnZengHui/tcptransport/src/framework/signal"
	"github.com/AnZengHui/tcptransport/src/proxyServer/network"
	"log"
	"os"
)

func main() {
	manager := network.ClientManager{}
	manager.StartUp()
	fmt.Println(manager)
	Loger.SetFilePath("log/login")
	s := sig.Wait(os.Interrupt, os.Kill)
	log.Printf("Got signal `%v` \n", s)
}
