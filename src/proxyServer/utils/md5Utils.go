package utils

import (
	"crypto/rand"
	"math/big"
	"sync"
)

func initID(max int64, min int64) []int64 {
	allConnectID := make([]int64, max)
	for i := 0; i < int(max); i++ {
		result, _ := rand.Int(rand.Reader, big.NewInt(max))
		allConnectID[i] = min + result.Int64()
	}
	return allConnectID
}

var connectUuids []int64

func init() {
	connectUuids = initID(100000, 10000)
}

var index = 0

var lock sync.Mutex

func NewUUID() int64 {
	lock.Lock()
	result := connectUuids[index]
	index += 1
	lock.Unlock()
	return result
}
