package network

import (
	"github.com/AnZengHui/tcptransport/src/framework/Loger"
	"net"
	"sync"
)

type ClientManager struct {
	Manager
	lock             sync.Mutex
	connectedClients map[string]*ConnectClient
	acceptChan       chan *ConnectClient
	closeChan        chan *ConnectClient
}

func (manager *ClientManager) onAccept(connectClient *ConnectClient) {

	if manager.checkConnect(connectClient.connectID) {
		manager.connectedClients[connectClient.connectID] = connectClient
		go connectClient.HandleConnect()
	} else {
		Loger.Debug("connectID is exits %s", connectClient.connectID)
	}
}
func (manager *ClientManager) StartUp() bool {
	manager.acceptChan = make(chan *ConnectClient)
	manager.closeChan = make(chan *ConnectClient)
	for {
		select {
		case accepted, err := <-manager.acceptChan:
			Loger.Println("accept:", accepted.connectID, accepted.Ptr.RemoteAddr())
			if !err {
				manager.onAccept(accepted)
			}

		case closed, err := <-manager.closeChan:
			if !err {
				manager.onClose(closed)
			}
		default:
			//
		}
	}
}

func (manager *ClientManager) checkConnect(connectID string) bool {
	_, ok := manager.connectedClients[connectID]
	return ok
}

func (manager *ClientManager) onClose(closeClient *ConnectClient) {
	if manager.checkConnect(closeClient.connectID) {
		manager.connectedClients[closeClient.connectID] = nil
	}
}

func (manager *ClientManager) Push(conn *net.TCPConn) {
	connectClient := NewConnectClient(conn)
	manager.acceptChan <- connectClient
}

func (manager *ClientManager) Close(closeClient *ConnectClient) {
	manager.closeChan <- closeClient
}
