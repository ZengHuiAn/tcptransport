package network

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"github.com/AnZengHui/tcptransport/src/framework/Loger"
	"github.com/AnZengHui/tcptransport/src/proxyServer/utils"
	"io"
	"net"
	"strconv"
)

type ClientHandler struct {
	readBuff *DataBuffer
	sendBuff *DataBuffer
}

//连接的客户端
type ConnectClient struct {
	ClientHandler
	Ptr       *net.TCPConn
	connectID string
}

func NewConnectClient(ptr *net.TCPConn) *ConnectClient {
	return &ConnectClient{ClientHandler{NewDataBuffer(10), NewDataBuffer(10)}, ptr, strconv.FormatInt(utils.NewUUID(), 10)}
}

func (client *ConnectClient) SendData(data []byte) {
	client.sendBuff.Push(data)
}

func (client *ConnectClient) StartUp() {
	for {
		select {
		case value := <-client.sendBuff.ch:
			fmt.Println("发送数据", value)
			_, _ = client.Ptr.Write(value)
		case readBuff := <-client.readBuff.ch:
			fmt.Println("读取数据", readBuff)
		default:
			//
		}
	}
}

func (client *ConnectClient) HandleConnect() {
	go client.StartUp()
	defer clientManager.Close(client)
	for {
		bs := make([]byte, 12)
		length, err := client.Ptr.Read(bs)
		//fmt.Println("读取数据")
		if err == io.EOF {
			Loger.Error(fmt.Sprintf("read error %v", err))
			return
		}
		if length == 0 {
			continue
		}
		buffer := &bytes.Buffer{}

		err = binary.Write(buffer, binary.LittleEndian, bs)
		if err != nil {
			Loger.Error(fmt.Sprintf("convert binary write error %v", err))
		}
		//先解析header
		header := &ClientHeader{}
		_ = binary.Read(buffer, binary.LittleEndian, header)
		//读取body
		body := make([]byte, header.Length-12)
		length, err = client.Ptr.Read(body)
		err = binary.Write(buffer, binary.LittleEndian, bs)
		err = binary.Write(buffer, binary.LittleEndian, body)
		if err != nil {
			Loger.Error(fmt.Sprintf("write error %v", err))
		}
		Loger.Info("push message ", buffer.Bytes())
		client.readBuff.Push(buffer.Bytes())
	}
}
