package network

type ClientHeader struct {
	Length    uint32
	MessageID uint32
	SN        uint32
}

func NewClientHeader(MessageID uint32, SN uint32) *ClientHeader {
	return &ClientHeader{Length: 12, MessageID: MessageID, SN: SN}
}
