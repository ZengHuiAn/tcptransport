package network

import "net"

type ServerListener struct {
	listener         *net.TCPListener
	maxListenerCount int64  //最大连接数量
	listenAddr       string // 监听地址
	currConnectCount int64
}

func GetNewServerCenter() ServerListener {

	server := ServerListener{}
	return server
}
