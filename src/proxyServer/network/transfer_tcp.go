package network

import (
	"fmt"
	"github.com/AnZengHui/tcptransport/src/framework/Loger"
	"net"
	"strings"
)

var server = GetNewServerCenter()

func StartTransfer(localAddr string) {
	adds := strings.Split(localAddr, ":")
	if adds[0] == "" {
		adds[0] = "0.0.0.0"
	}
	addrLocal := strings.Join(adds, ":")
	listenAddr := addrLocal
	maxListenerCount := 10
	currConnectCount := 0
	addr, err := net.ResolveTCPAddr("tcp", listenAddr)

	if err != nil {
		panic(fmt.Sprintf("create tcp error `%v`", err))
	}

	Loger.Info("listen tcp localhost:%v", addr)
	listener, err := net.ListenTCP("tcp", addr)
	defer func() {
		_ = listener.Close()
	}()
	if err != nil {
		panic(fmt.Sprintf("listen error %v", err))
	}

	StartUp()
	for {
		if currConnectCount >= maxListenerCount {
			Loger.Printf("达到连接最大上限:%v %v", currConnectCount, maxListenerCount)
		} else {
			fmt.Println("等待连接")
		}

		client, err := listener.AcceptTCP()
		currConnectCount++
		if err != nil {
			Loger.Error("accept error %v", err)
		}

		Loger.Debug("客户端连接 %v count %v", client.RemoteAddr(), currConnectCount)
		clientManager.Push(client)
	}
}

func StartUp() {
	go clientManager.StartUp()
}

var clientManager = &ClientManager{}
