package network

import "github.com/AnZengHui/tcptransport/src/framework/Loger"

//数据缓冲区
/*
数据缓冲区
*/
type DataBuffer struct {
	ch chan []byte // 数据的缓冲区

}

func NewDataBuffer(count int8) *DataBuffer {
	return &DataBuffer{ch: make(chan []byte, count)}
}

func (this *DataBuffer) Push(buff []byte) {

	defer func() {
		//
		if err := recover(); err != nil {
			//
			Loger.Debug("channel closed %v", err)
		}
	}()
	this.ch <- buff
}

func (this *DataBuffer) Pop() []byte {
	defer func() {
		//
		if err := recover(); err != nil {
			//
			Loger.Debug("channel closed %v", err)
		}
	}()

	return <-this.ch
}

func (this *DataBuffer) Close() {
	defer func() {
		//
		if err := recover(); err != nil {
			//
			Loger.Debug("channel closed %v", err)
		}
	}()

	close(this.ch)
}
