package main

import (
	"github.com/AnZengHui/tcptransport/src/framework/Loger"
	sig "github.com/AnZengHui/tcptransport/src/framework/signal"
	"github.com/AnZengHui/tcptransport/src/proxyServer/network"
	"log"
	"os"
)

func main() {
	Loger.SetFilePath("log/gateway")
	network.StartTransfer(":10000")
	s := sig.Wait(os.Interrupt, os.Kill)
	log.Printf("Got signal `%v` \n", s)
}
